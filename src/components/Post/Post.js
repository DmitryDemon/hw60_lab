import React from 'react';
import './Post.css';

const Post = (props) => {
  return (
    <div className="Post">
      <p className="mess">
        <span className='author'>{props.author}</span>
        {props.value}
        <span className="datetime">{props.datetime}</span>
      </p>
    </div>
  );
};


export default Post;