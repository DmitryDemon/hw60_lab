import React from 'react';
import './Form.css';
import Button from '../Button/Button'

const Form = (props) => {
  return (
    <div className="formMess">
      <form>
        <label className="box" htmlFor="author">Author:</label>
        <input value={props.author} onChange={props.change} className="box box-author" type="text" name="author" />

          <label className="box" htmlFor="message">Message:</label>
          <textarea value={props.message} onChange={props.change} className="box-mess" name="message" />

            <Button
              btnType="Success"
              onClick={props.onClick}
            >Send</Button>
      </form>
    </div>
  );
};

export default Form;