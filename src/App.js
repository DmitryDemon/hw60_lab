import React, { Component } from 'react';
import './App.css';
import Post from "./components/Post/Post";
import Form from "./components/Form/Form";

class App extends Component {

  state = {
    posts: [],
    message: '',
    author: '',
    isLoading: true
  };

  shouldComponentUpdate(nextProps, nextState){
    return (this.state.posts.length !== nextState.posts.length ||
        this.state.message !== nextState.message ||
        this.state.author !== nextState.author)
  }

  changeHandler = (event) => {
      this.setState({[event.target.name]: event.target.value})
  };

  loadData =(dateTime) => {
      const getMessegesUrl = 'http://146.185.154.90:8000/messages';
    const getLastMessagesUrl = 'http://146.185.154.90:8000/messages?datetime='+dateTime;
    fetch(dateTime? getLastMessagesUrl : getMessegesUrl).then(response => {
      if (response.ok) {
        return response.json()
      }
      throw new Error('Something wrong with request');
    }).then(posts => {
        this.setState({posts: [...this.state.posts].concat(posts)});
    }).catch(error => {
      console.log(error);
    })
  };

  addPost = (e) => {
    e.preventDefault();
    const url = 'http://146.185.154.90:8000/messages';
      if (this.state.author && this.state.message){
          const data = new URLSearchParams();
          data.set('message', this.state.message);
          data.set('author', this.state.author);
          this.setState({message: ''}, ()=> {
              fetch(url, {
                  method: 'post',
                  body: data
              })
          })
      }
  };

  componentDidMount() {
      this.loadData();
      this.setState({isLoading: false});
  }

  componentDidUpdate(){
      clearInterval(this.interval);
      const lastDatetime = this.state.posts[this.state.posts.length -1].datetime;
      if (lastDatetime) this.interval = setInterval(this.loadData, 2000, lastDatetime);
  }

  mapPosts = () => {
    return this.state.posts.map((post, key) => {
      return <Post key={key} value={post.message} author={post.author} datetime={post.datetime}/>
    })
  };

  render() {
    return (
      <div className="container App">
      {this.state.isLoading ? <div id="preloader">Preloader</div>:
        <div className="Posts">{this.state.posts.length > 0? this.mapPosts().reverse() : "Whait!!!"}</div>}
          <Form
          message={this.state.message}
          author={this.state.author}
          change={(e) => this.changeHandler(e)}
          onClick={(e) => this.addPost(e)}/>
      </div>
    );
  }
}

export default App;
